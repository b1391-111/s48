import React from 'react';

// react-router
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'

// CSS
import './App.css';

// Components
import Navbar from './components/Navbar/Navbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Courses from './pages/Courses';
import Banner from './components/Banner/Banner';

function App() {
  return (
    <Router>
      <Navbar />    
      <Switch>

        <Route path="/" exact>
          <Home />
        </Route>

        <Route path="/courses">
          <Courses />
        </Route>

        <Route path="/login">
          <Login />
        </Route>

        <Route path="/register">
          <Register />
        </Route>
        
        <Route path="/*">
          <Banner 
            title="Page Not Found!"
            linkTo="/"
            btnInfo="Back to homepage"
          />
        </Route>

      </Switch>
    </Router>
  );
}

export default App;