import React from 'react';

// react-bootstrap components
import {
    Button
} from 'react-bootstrap';

// react-router
import { Link } from 'react-router-dom';

const Banner = ({title, subtitle, btnInfo, linkTo}) => {
    return (
        <div className="jumbotron">
            <h1>{title}</h1>
            <p>{subtitle}</p>
            <Button as={Link} to={linkTo} variant="info">{btnInfo}</Button>
        </div>
    )
}

export default Banner;