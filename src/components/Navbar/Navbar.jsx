import React, { useState, useEffect } from 'react';

// react-router
import { Link, useHistory } from 'react-router-dom';

// react-bootstrap components
import {
    Navbar,
    Nav
} from 'react-bootstrap';


const Header = () => {
    const email = localStorage.getItem("email");
    const [user, setUser] = useState(email);
    const history = useHistory();

    useEffect(() => {
        if(email){
            setUser(email)
        } else {
            setUser(null)
        }
    }, [email])

    const handleLogout = () => {
        localStorage.clear();
        history.push('/');
        setUser(null);
        alert("Logged Out")
    }
    return (
        <Navbar bg="info" expand="lg">
            <Navbar.Brand href="#home">Course Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
                    {
                        user ? (
                            <>
                                <Nav.Link onClick={handleLogout}>Logout</Nav.Link>
                            </>
                        ) : (
                            <>
                                <Nav.Link as={Link} to="/login">Login</Nav.Link>
                                <Nav.Link as={Link} to="/register">Register</Nav.Link>
                            </>
                        )
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default Header;
