import React from 'react'

// Components
import Banner from '../components/Banner/Banner';
import HighLights from '../components/Highlights/Highlights';

const Home = () => {
    return (
        <>
            <Banner 
                title="Zuitt Coding Bootcamp" 
                subtitle="Opportunities for everyone, everywhere!"
                linkTo="/courses"
                btnInfo="Enroll Now!"    
            />
            <HighLights />
        </>
    )
}

export default Home
