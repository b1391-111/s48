import React, { useState, useEffect } from 'react';

// react-bootstrap components
import { 
    Button, 
    Col, 
    Container, 
    Form, 
    Row 
} from 'react-bootstrap';

const Register = () => {
    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirm: ""
    });
    const [isDisabled, setIsDisabled] = useState(true)
    
    // Form Data destructuring
    const { firstName, lastName, email, password, confirm } = formData;
    
    // Use Effect
    useEffect(() => {
        if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && confirm !== "")) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [firstName, lastName, email, password, confirm])
    

    // Events Handler Functions
    const handleChange = (e) => setFormData({...formData, [e.target.name]: e.target.value});

    const handleSubmit = (e) => {
        e.preventDefault();
        
        if(firstName.length < 3) {
            alert("First Name should be 3 or more letters")
        } else if(lastName.length < 3) {
            alert("Last Name should be 3 or more letters")
        } else if(email.length < 8) {
            alert("Invalid Email")
        } else if(password.length < 8){
            alert("Password should be 8 or more letters")
        } else if(password !== confirm) {
            alert("Confirm Password and password does not match!");
        } else {
            console.log(formData);
            setFormData({
                firstName: "",
                lastName: "",
                email: "",
                password: "",
                confirm: ""
            })
        }
    }

    return (
        <Container className="my-5">
            <h1 className="text-center">Register</h1>
            <Row className="justify-content-center mt-4">
                <Col xs={10} md={6}>
                    <Form
                        className="border p-3"
                        onSubmit={handleSubmit}
                    >
                        <Form.Group 
                            className="mb-3" 
                            controlId="fName"
                        >
                            <Form.Label>First Name</Form.Label>
                            <Form.Control 
                                onChange={handleChange}
                                value={formData.firstName}
                                name="firstName"
                                type="text" 
                                placeholder="Enter your First Name..." 
                            />
                        </Form.Group>

                        <Form.Group 
                            className="mb-3" 
                            controlId="lName"
                        >
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control 
                                onChange={handleChange}
                                value={formData.lastName}
                                name="lastName"
                                type="text" 
                                placeholder="Enter your Last Name..." 
                            />
                        </Form.Group>

                        <Form.Group 
                            className="mb-3" 
                            controlId="email"
                        >
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                onChange={handleChange}
                                value={formData.email}
                                name="email"
                                type="email" 
                                placeholder="Enter email" 
                            />
                        </Form.Group>

                        <Form.Group 
                            className="mb-3" 
                            controlId="password"
                        >
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                onChange={handleChange}
                                value={formData.password}
                                name="password"
                                type="password"
                                placeholder="Password"
                            />
                        </Form.Group>

                        <Form.Group 
                            className="mb-3" 
                            controlId="confirmPassword"
                        >
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control 
                                onChange={handleChange}
                                value={formData.confirm}
                                name="confirm"
                                type="password" 
                                placeholder="Confirm Password" 
                            />
                        </Form.Group>

                        <Button 
                            variant="primary" 
                            type="submit"
                            className="btn-info"
                            disabled={isDisabled}
                        >
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default Register;